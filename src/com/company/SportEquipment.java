package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by ashap on 30.3.17.
 */

public class SportEquipment {

    private static Locale.Category category;
    private static String title;
    private static int price;
    static String catalogList;
    static String bikeList=" ";
    static String skateList=" ";
    static String skateboardsList=" ";


    public static String ShowCatalog() {
        catalogList = "";
        Map catalog = new HashMap();
        catalog.put(1, "Bikes");
        catalog.put(2, "Skates");
        catalog.put(3, "Scateboards");
        for (int i = 1; i < catalog.size() + 1; i++) {
            String temp = i + " " + catalog.get(i).toString() + '\n';
            catalogList = catalogList.concat(temp);

        }
        catalogList = catalogList + '\n' + "Enter the number of selected category";
        return catalogList;
    }

    public static String bikes() {
        Map bikes_cat = new HashMap();
        bikes_cat.put(1, "Evolution 26 Men's Bike, price: 3$ ");
        bikes_cat.put(2, "Mongoose 27.5 Inch Mens Terrex Mountain Bike, price: 10$ ");
        bikes_cat.put(3, "Kent Takara Renzo 700C, price^ 7$");
        bikes_cat.put(4, "Kawasaki K26G 26" + " Hardtail Bike, price: 6$");
        bikes_cat.put(5, "Huffy Rock Creek 24 Inch Bicycle, price: 5$");
        for (int i = 1; i < bikes_cat.size() + 1; i++) {
            String temp = i + " " + bikes_cat.get(i + " (in a day)").toString() + '\n';
            catalogList = catalogList.concat(temp);
        }
        bikeList = bikeList + '\n' + "Enter 's' to add item to your basket";
        return bikeList;
    }

    public static String skates() {
        Map skates_cat = new HashMap();
        skates_cat.put(1, "Nike NFL Custom Outdoor, price: 3$");
        skates_cat.put(2, "Adidas shoes transformed, price: 6$");
        skates_cat.put(3, "Cyclone Blue Titan, price: 2$");
        for (int i = 1; i < skates_cat.size() + 1; i++) {
            String temp = i + " " + skates_cat.get(i + " (in a day)").toString() + '\n';
            catalogList = catalogList.concat(temp);
        }
        skateList = skateList + '\n' + "Enter 's' to add item to your basket";
        return skateList;
    }

    public static String skateboards() {
        Map skateboards_cat = new HashMap();
        skateboards_cat.put(1, "Z-Flex Skateboards Roundtail Island Time Complete, price: 4$");
        skateboards_cat.put(2, "Remember Vibe Dingus Complete Longboard Skateboard, price: 2$");
        skateboards_cat.put(3, "Z-Flex Skateboards Pintail Tie Dye / Black / White / Purple, price: 3$");
        skateboards_cat.put(4, "Penny Skateboards Black Gold 22" +" Complete Cruiser, price: 5$");
        for (int i = 1; i <skateboards_cat.size() + 1; i++) {
            String temp = i + " " + skateboards_cat.get(i + " (in a day)").toString() + '\n';
            catalogList = catalogList.concat(temp);
        }
        skateboardsList = skateboardsList + '\n' + "Enter 's' to add item to your basket";
        return skateboardsList;
    }
}